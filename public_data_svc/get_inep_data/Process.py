from public_data_svc.Base import Base
from pkg.utils.execution.Process import Process as BaseProcess


class Process(BaseProcess):

    def __init__(self):
        super().__init__()
        self.base = Base()

    def execute_process(self) -> None:
        self.logger.info(f"Start of data processing...")

        try:
            self.base.file_manager.get_file_from_url(self.base.census_url, f"{self.base.files_path}/sample.zip", 1000)
        except Exception as err:
            self.logger.error(f"Could not download file due to: {err}")
            raise err

        try:
            self.base.file_manager.unpack_files(self.base.files_path, self.base.unpack_to)
        except Exception as err:
            self.logger.error(f"Could not unpack file due to: {err}")
            raise err

        try:
            self.base.file_manager.separate_required_files(f"{self.base.files_path}/{self.base.unpack_to}",
                                                           self.base.prefix_files,
                                                           self.base.required_data_dir)
        except Exception as err:
            self.logger.error(f"Could not separate required files due to: {err}")
            raise err

        self.logger.info(f"End of data processing...")
