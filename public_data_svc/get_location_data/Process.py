from public_data_svc.Base import Base
from pkg.utils.execution.Process import Process as BaseProcess
from pyspark.sql.functions import col
from pyspark.sql.functions import upper
from pyspark.sql.functions import when
from pyspark.sql.types import IntegerType


class Process(BaseProcess):

    def __init__(self):
        super().__init__()
        self.base = Base()

    def execute_process(self) -> None:
        self.logger.info(f"Start of data processing (get location data from pubic sources)...")

        cities_path = f"{self.base.location_data_path}/cities.csv"
        states_path = f"{self.base.location_data_path}/states.csv"
        self.base.path_manager.create_directory_if_not_exist(self.base.location_data_path)

        try:
            self.base.file_manager.get_file_from_url(self.base.location_cities_url, cities_path, 1000)
            self.base.file_manager.get_file_from_url(self.base.location_states_url, states_path, 1000)
        except Exception as err:
            self.logger.error(f"Could not download file due to: {err}")
            raise err

        # Open cities file and write to DB
        sdf = self.base.spark_file_manager.open_csv_file(cities_path, ";", header=True)
        self.base.db.insert_spark_df_to_table(sdf, self.base.cities_raw_table, False, 'overwrite')

        # Open states file and write to DB
        sdf = self.base.spark_file_manager.open_csv_file(states_path, ";", header=True)
        self.base.db.insert_spark_df_to_table(sdf, self.base.states_raw_table, False, 'overwrite')

        # Transform Location Data
        # CITIES
        sdf_cities = self.base.db.read_from_db(self.base.cities_raw_table)
        sdf_cities_treated = sdf_cities.select(col("IBGE7").alias("ibge"),
                                               col("UF").alias("uf"),
                                               upper(col("Município")).alias("nm_municipio"),
                                               upper(col("Região")).alias("nm_regiao"),
                                               col("Porte").alias("porte_municipio"),
                                               upper(col("Capital")).alias("capital"))
        sdf_cities_treated = sdf_cities_treated.withColumn("flag_capital", when(sdf_cities_treated.capital == "CAPITAL",
                                                                                True).otherwise(False)).drop("capital")
        sdf_cities_treated = sdf_cities_treated.withColumn("cod_ibge",
                                                           sdf_cities_treated["ibge"].cast(IntegerType())).drop("ibge")
        self.base.db.insert_spark_df_to_table(sdf_cities_treated, self.base.cities_treated, False)

        # STATES
        sdf_states = self.base.db.read_from_db(self.base.states_raw_table)
        sdf_states_treated = sdf_states.select(col("IBGE").alias("ibge"),
                                               col("UF").alias("uf"),
                                               upper(col("Região")).alias("nm_regiao"),
                                               col("Qtd Mun").alias("qtd"))
        sdf_states_treated = sdf_states_treated.withColumn("cod_ibge",
                                                           sdf_states_treated["ibge"].cast(IntegerType())).drop("ibge")
        sdf_states_treated = sdf_states_treated.withColumn("qtd_municipios",
                                                           sdf_states_treated["qtd"].cast(IntegerType())).drop("qtd")
        self.base.db.insert_spark_df_to_table(sdf_states_treated, self.base.states_treated, False)

        self.logger.info(f"End of data processing...")
