from public_data_svc.Base import Base
from pkg.utils.execution.Process import Process as BaseProcess
from pyspark.sql.functions import col
from pyspark.sql.functions import concat
from pyspark.sql.functions import lit
from pyspark.sql.types import IntegerType


class Process(BaseProcess):

    def __init__(self):
        super().__init__()
        self.base = Base()

    def execute_process(self) -> None:
        self.logger.info(f"Start of data processing for generating facts and dimensions for census insights...")

        # build fact table
        sdf = self.base.db.read_from_db(self.base.census_raw_table)
        sdf_filtered = sdf.select(col("ID_ALUNO").alias("id_aluno"),
                                  col("ID_MATRICULA").alias("id_matricula"),
                                  col("CO_MUNICIPIO_END").alias("c_municipio"),
                                  col("CO_UF_END").alias("uf_endereco"),
                                  col("NU_MES").alias("mes_nascimento"),
                                  col("NU_ANO").alias("ano_nascimento"),
                                  concat(col("NU_MES"), lit("-"), col("NU_ANO")).alias("mes_ano_nascimento"),
                                  col("NU_IDADE").alias("idade"),
                                  col("TP_SEXO").alias("sexo"),
                                  col("TP_COR_RACA").alias("cor_raca"),
                                  col("TP_ETAPA_ENSINO").alias("etapa_ensino")).distinct()
        sdf_filtered = sdf_filtered.withColumn("cod_municipio", sdf_filtered["c_municipio"].cast(IntegerType())).drop(
            "c_municipio")
        sdf_filtered = sdf_filtered.withColumn("cod_uf", sdf_filtered["uf_endereco"].cast(IntegerType())).drop(
            "uf_endereco")
        sdf_filtered = sdf_filtered.withColumn("idade_atual", sdf_filtered["idade"].cast(IntegerType())).drop("idade")
        sdf_filtered = sdf_filtered.withColumn("sexo_id", sdf_filtered["sexo"].cast(IntegerType())).drop(
            "sexo")
        sdf_filtered = sdf_filtered.withColumn("cor_raca_id", sdf_filtered["cor_raca"].cast(IntegerType())).drop(
            "cor_raca")
        sdf_filtered = sdf_filtered.withColumn("etapa_ensino_id",
                                               sdf_filtered["etapa_ensino"].cast(IntegerType())).drop(
            "etapa_ensino")
        self.base.db.insert_spark_df_to_table(sdf_filtered, self.base.fact_students, False)

        # build dimensions
        sdf_race = self.base.db.read_from_db(self.base.students_races)
        self.base.db.insert_spark_df_to_table(sdf_race, self.base.dim_race, False)

        sdf_gender = self.base.db.read_from_db(self.base.students_gender)
        self.base.db.insert_spark_df_to_table(sdf_gender, self.base.dim_gender, False)

        sdf_education_lvl = self.base.db.read_from_db(self.base.education_level)
        self.base.db.insert_spark_df_to_table(sdf_education_lvl, self.base.dim_education_level, False)

        sdf_states = self.base.db.read_from_db(self.base.states_treated)
        self.base.db.insert_spark_df_to_table(sdf_states, self.base.dim_states, False)

        sdf_cities = self.base.db.read_from_db(self.base.cities_treated)
        self.base.db.insert_spark_df_to_table(sdf_cities, self.base.dim_cities, False)

        self.logger.info(f"End of data processing...")
