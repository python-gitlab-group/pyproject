from pkg.utils.file_manager import FileManager
from pkg.utils.file_manager import FileSparkManager
from pkg.utils.path_manager import PathManager
from pkg.db import Postgresql
from pkg.logging.LogManager import LogManager as LM
from config import CONFIG


class Base:
    """
        The Base class for data processing.
        Here we import all the process dependencies.
    """

    def __init__(self):
        if CONFIG.db.type == "postgres":
            self.db = Postgresql.Postgresql()
        self.file_manager = FileManager.FileManager()
        self.spark_file_manager = FileSparkManager.FileSparkManager()
        self.path_manager = PathManager.PathManager()
        self.files_path = CONFIG.files_path
        self.unpack_to = CONFIG.unpack_to
        self.census_url = CONFIG.inep.census_url
        self.location_cities_url = CONFIG.location.cities_url
        self.location_states_url = CONFIG.location.states_url
        self.prefix_files = CONFIG.inep.prefix_required_files
        self.required_data_dir = CONFIG.required_data_directory
        self.location_path = CONFIG.location_path
        self.required_data_path = f"{self.files_path}/{self.unpack_to}/{self.required_data_dir}"
        self.location_data_path = f"{self.files_path}/{self.unpack_to}/{self.location_path}"
        self.logger = LM.get_logger(__name__)

        # TODO: Change this approach to a DAO/DTO, preference for an ORM.
        # NOTE: It was not implemented yet because I had no time.
        # schemas
        self.sc_ingestion = CONFIG.db.schemas.ingestion.name
        self.sc_distillation = CONFIG.db.schemas.distillation.name
        self.sc_insights = CONFIG.db.schemas.insights.name

        # qualified table names
        self.census_raw_table = f"{self.sc_ingestion}.{CONFIG.db.tables.census.name}"
        self.cities_raw_table = f"{self.sc_ingestion}.{CONFIG.db.tables.cities.name}"
        self.states_raw_table = f"{self.sc_ingestion}.{CONFIG.db.tables.states.name}"
        self.students_gender = f"{self.sc_distillation}.{CONFIG.db.tables.students_gender.name}"
        self.students_races = f"{self.sc_distillation}.{CONFIG.db.tables.students_races.name}"
        self.states_treated = f"{self.sc_distillation}.{CONFIG.db.tables.states.name}"
        self.cities_treated = f"{self.sc_distillation}.{CONFIG.db.tables.cities.name}"
        self.education_level = f"{self.sc_distillation}.{CONFIG.db.tables.education_level.name}"

        # data models objects
        self.fact_students = f"{self.sc_insights}.{CONFIG.db.tables.fact_students.name}"
        self.dim_states = f"{self.sc_insights}.{CONFIG.db.tables.dim_states.name}"
        self.dim_cities = f"{self.sc_insights}.{CONFIG.db.tables.dim_cities.name}"
        self.dim_race = f"{self.sc_insights}.{CONFIG.db.tables.dim_students_races.name}"
        self.dim_gender = f"{self.sc_insights}.{CONFIG.db.tables.dim_students_genders.name}"
        self.dim_education_level = f"{self.sc_insights}.{CONFIG.db.tables.dim_education_level.name}"
