from public_data_svc.Base import Base
from pkg.utils.execution.Process import Process as BaseProcess


class Process(BaseProcess):

    def __init__(self):
        super().__init__()
        self.base = Base()

    def execute_process(self) -> None:
        self.logger.info(f"Start of data processing...")

        # Open files and write the raw content to DB
        files = self.base.path_manager.get_all_files_in_dir(self.base.required_data_path, ".CSV")

        i = 0
        write_mode = "overwrite"
        for file in files:
            if i > 0:
                write_mode = "append"
            sdf = self.base.spark_file_manager.open_csv_file(file, "|", header=True)

            self.base.db.insert_spark_df_to_table(sdf, self.base.census_raw_table, False, write_mode)
            i += 1
        self.logger.info(f"End of data processing...")
