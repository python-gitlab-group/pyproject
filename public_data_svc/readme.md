## Pipeline flow

### CENSUS INEP DATA FLOW

1. Get cities and states data from public source and write it to database.
```
python controller.py --service public_data_svc --process get_location_data --service_root_path .
```

2. Download, unpack and select most recent INEP microdata. 
```
python controller.py --service public_data_svc --process get_inep_data --service_root_path .
```

3. Read selected files and write it to ingestion schema (landing area for raw data).
```
python controller.py --service public_data_svc --process parse_store_inep_data --service_root_path .
```

4. Transform data to generate a star schema with dimensions and facts so data can be easily analyzed by other teams.
```
python controller.py --service public_data_svc --process generate_census_insights --service_root_path .
```
