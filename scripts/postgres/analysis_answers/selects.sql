select
       c.nm_municipio
       ,count(f.id_aluno) qtd_alunos
from insights.fact_students f
left join insights.dim_cities c on c.cod_ibge = f.cod_municipio
left join insights.dim_education_level l on l.level_id = f.etapa_ensino_id
where l.level_id = 41
group by c.nm_municipio
order by count(f.id_aluno) desc;

select
       r.race_description
       ,count(f.id_aluno) qtd_alunos
from insights.fact_students f
left join insights.dim_students_races r on r.race_id = f.cor_raca_id
group by r.race_description
order by count(f.id_aluno) desc;
