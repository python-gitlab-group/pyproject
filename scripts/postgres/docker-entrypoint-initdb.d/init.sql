create schema ingestion;
create schema distillation;
create schema insights;

create table distillation.students_gender (
    gender_id int primary key,
    gender_description VARCHAR(255) not null,
    gender_abbreviation VARCHAR(1) not null
);

insert into distillation.students_gender VALUES(1, 'Masculino', 'M');
insert into distillation.students_gender VALUES(2, 'Feminino', 'F');
create index on distillation.students_gender(gender_id);

create table distillation.students_races (
    race_id int primary key,
    race_description VARCHAR(20) not null
);

insert into distillation.students_races VALUES(0, 'Não declarada');
insert into distillation.students_races VALUES(1, 'Branca');
insert into distillation.students_races VALUES(2, 'Preta');
insert into distillation.students_races VALUES(3, 'Parda');
insert into distillation.students_races VALUES(4, 'Amarela');
insert into distillation.students_races VALUES(5, 'Indigena');
create index on distillation.students_races(race_id);

create table distillation.education_level (
    level_id int primary key,
    level_description VARCHAR(20) not null
);

insert into distillation.education_level VALUES(1,'Educação Infantil - Creche');
insert into distillation.education_level VALUES(2,'Educação Infantil - Pré-escola');
insert into distillation.education_level VALUES(4,'Ensino Fundamental de 8 anos - 1ª Série');
insert into distillation.education_level VALUES(5,'Ensino Fundamental de 8 anos - 2ª Série');
insert into distillation.education_level VALUES(6,'Ensino Fundamental de 8 anos - 3ª Série');
insert into distillation.education_level VALUES(7,'Ensino Fundamental de 8 anos - 4ª Série');
insert into distillation.education_level VALUES(8,'Ensino Fundamental de 8 anos - 5ª Série');
insert into distillation.education_level VALUES(9,'Ensino Fundamental de 8 anos - 6ª Série');
insert into distillation.education_level VALUES(10,'Ensino Fundamental de 8 anos - 7ª Série');
insert into distillation.education_level VALUES(11,'Ensino Fundamental de 8 anos - 8ª Série');
insert into distillation.education_level VALUES(14,'Ensino Fundamental de 9 anos - 1º Ano');
insert into distillation.education_level VALUES(15,'Ensino Fundamental de 9 anos - 2º Ano');
insert into distillation.education_level VALUES(16,'Ensino Fundamental de 9 anos - 3º Ano');
insert into distillation.education_level VALUES(17,'Ensino Fundamental de 9 anos - 4º Ano');
insert into distillation.education_level VALUES(18,'Ensino Fundamental de 9 anos - 5º Ano');
insert into distillation.education_level VALUES(19,'Ensino Fundamental de 9 anos - 6º Ano');
insert into distillation.education_level VALUES(20,'Ensino Fundamental de 9 anos - 7º Ano');
insert into distillation.education_level VALUES(21,'Ensino Fundamental de 9 anos - 8º Ano');
insert into distillation.education_level VALUES(41,'Ensino Fundamental de 9 anos - 9º Ano');
insert into distillation.education_level VALUES(25,'Ensino Médio - 1º ano/1ª Série');
insert into distillation.education_level VALUES(26,'Ensino Médio - 2º ano/2ª Série');
insert into distillation.education_level VALUES(27,'Ensino Médio - 3ºano/3ª Série');
insert into distillation.education_level VALUES(28,'Ensino Médio - 4º ano/4ª Série');
insert into distillation.education_level VALUES(29,'Ensino Médio - Não Seriada');
insert into distillation.education_level VALUES(30,'Curso Técnico Integrado (Ensino Médio Integrado) 1ª Série');
insert into distillation.education_level VALUES(31,'Curso Técnico Integrado (Ensino Médio Integrado) 2ª Série');
insert into distillation.education_level VALUES(32,'Curso Técnico Integrado (Ensino Médio Integrado) 3ª Série');
insert into distillation.education_level VALUES(33,'Curso Técnico Integrado (Ensino Médio Integrado) 4ª Série');
insert into distillation.education_level VALUES(34,'Curso Técnico Integrado (Ensino Médio Integrado) Não Seriada');
insert into distillation.education_level VALUES(35,'Ensino Médio - Modalidade Normal/Magistério 1ª Série');
insert into distillation.education_level VALUES(36,'Ensino Médio - Modalidade Normal/Magistério 2ª Série');
insert into distillation.education_level VALUES(37,'Ensino Médio - Modalidade Normal/Magistério 3ª Série');
insert into distillation.education_level VALUES(38,'Ensino Médio - Modalidade Normal/Magistério 4ª Série');
insert into distillation.education_level VALUES(39,'Curso Técnico - Concomitante');
insert into distillation.education_level VALUES(40,'Curso Técnico - Subsequente');
insert into distillation.education_level VALUES(65,'EJA - Ensino Fundamental - Projovem Urbano');
insert into distillation.education_level VALUES(67,'Curso FIC integrado na modalidade EJA  - Nível Médio');
insert into distillation.education_level VALUES(68,'Curso FIC Concomitante ');
insert into distillation.education_level VALUES(69,'EJA - Ensino Fundamental - Anos Iniciais');
insert into distillation.education_level VALUES(70,'EJA - Ensino Fundamental - Anos Finais');
insert into distillation.education_level VALUES(71,'EJA - Ensino Médio');
insert into distillation.education_level VALUES(72,'EJA - Ensino Fundamental  - Anos iniciais e Anos finais5');
insert into distillation.education_level VALUES(73,'Curso FIC integrado na modalidade EJA - Nível Fundamental (EJA integrada à Educação Profissional de Nível Fundamental) ');
insert into distillation.education_level VALUES(74,'Curso Técnico Integrado na Modalidade EJA (EJA integrada à Educação Profissional de Nível Médio)       - Não aplicável para turmas de atendimento educacional especializado (AEE) e atividade complementar');
create index on distillation.education_level(level_id);
