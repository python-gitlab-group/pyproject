"""
Contains the Service class for finding and handling Processes
"""
import os

from pkg.utils.path_manager.PathManager import PathManager
from pkg.utils import Constants


class Service:
    """
    Service process class.
    """

    @staticmethod
    def get_all_services() -> [str]:
        """
        Fetches all the service names by traversing the directory file structure.
        Returns:
            list: list of all valid services.
        """
        root_path = PathManager.get_root_path(__file__)
        root_dir = PathManager.get_root_dir_name(__file__)
        services = PathManager.get_folders_having_filename(
            root_path, 'Service.py')
        if root_dir in services:
            services.remove(root_dir)
        return services

    @staticmethod
    def get_all_processes(service: str) -> [str]:
        """
        Fetches all the process names for the service name passed as parameter by traversing the directory file structure.
        Args:
            service: pass the output service name
        Returns:
            list: list of all valid processes.
        """
        service = service.lower()
        root_path = PathManager.get_root_path(__file__)
        folder_path = PathManager.get_full_file_name(root_path, service)
        process = list(map(lambda ls: ".".join(
            ls), PathManager.get_subfolders_having_filename(folder_path, 'Process.py')))
        return process

    @staticmethod
    def get_all_service_groups() -> dict:
        group_dict = {}
        services = Service.get_all_services()

        for service in services:
            group_dict[service] = Service.get_all_processes(service)

        return group_dict

    @staticmethod
    def get_all_locales(service: str) -> [str]:
        """
        Fetches all the locale names for the service.
        Args:
            service (str): The service name.
        Returns:
            list: List of all valid locales for the given service.
        """
        service = service.lower()
        root_path = PathManager.get_root_path(__file__)
        configs = PathManager.get_all_files_in_dir(os.path.join(root_path, 'configs', 'services'), ext=".json")
        configs = [os.path.basename(c) for c in configs if os.path.basename(c).startswith(service)]
        locales = []
        for config in configs:
            if Constants.LOCALE_DELIMITER not in config:
                continue
            locale = config.replace(".json", "").split(Constants.LOCALE_DELIMITER)[-1]
            locales.append(locale)
        return locales

    @staticmethod
    def get_all_locale_groups() -> dict:
        """
        Fetches the list of locales for all available services.
        Returns:
            dict: Service name (key) mapped to list of locales (value).
        """
        group_dict = {}
        services = Service.get_all_services()

        for service in services:
            group_dict[service] = Service.get_all_locales(service)

        return group_dict
