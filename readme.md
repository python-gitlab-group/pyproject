# PyProject

Sample pyproject

## Authors
- Esequiel Virtuoso

## Data Flow
The following Data Flow is the local scenario where we use Postgres to simulate the Data Lake.
That's not a production approach once it should be a storage on a Cloud Platform.
![alt text](./doc/images/inep_data_flow_pg.png?raw=true)

The following Data Flow is a production scenario that needs more time to develop.
Here we use Airflow to control the data pipeline. Also, we run this over a cluster of Nodes.
![alt text](./doc/images/inep_data_flow.png?raw=true)

# Controller quick guideline
The controller and the config manager have been created, so we can easily develop new services with data transformation and run it calling the controller.

Every folder (within the project) that contains a `Service.py` file is recognized as a service.

Every sub-folder within this service folder that contains a `Process.py` file is recognized as a process.

Thinking in a data pipeline we can call it from Airflow dags or even in an Argo yml config file.

### Running locally
Within the project's root folder you must define the `PYTHON_PATH` env as follows.
```
export PYTHON_PATH=.
```

Create the db environment.
```
make env
```

Now, you can run a sequence of processes as you need.
```
python controller.py --service public_data_svc --process get_location_data --service_root_path .
python controller.py --service public_data_svc --process get_inep_data --service_root_path .
python controller.py --service public_data_svc --process parse_store_inep_data --service_root_path .
python controller.py --service public_data_svc --process generate_census_insights --service_root_path .
```

# Guidelines/best practices

## PEP8 Coding Standards
https://realpython.com/python-pep8/

## PEP484 Coding Standards
https://www.python.org/dev/peps/pep-0484/
https://realpython.com/python-type-checking/

To start getting code files into a clean and standardised structure we have
started following some guidelines. This is the current guideline that we are
going to try to follow:

https://jeffknupp.com/blog/2013/08/16/open-sourcing-a-python-project-the-right-way/

- all directories should follow the snake_case naming convention.
- all classes are defined using camelCase.
- all functions and methods follow the snake_case convention.

#### Types of logger outputs:
- **INFO**: to be used when monitoring progress of code and outputs/success of key steps
- **DEBUG**: to be used when troubleshooting
- **WARNING**: to be used when output of test not perfect, needs to be checked, but code allowed to continue
- **CRIT**: to be used when output of test bad, inside try clause
- **ERROR**: to be used inside except statement; leads to exit from code
