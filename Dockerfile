FROM python:3.7-stretch as base
WORKDIR .

FROM base as dependencies
ENV PYTHONPATH "${PYTHONPATH}:."
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
