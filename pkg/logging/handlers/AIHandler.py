from applicationinsights.logging import LoggingHandler
from applicationinsights import channel
from applicationinsights.exceptions import enable as enable_exceptions
from config import CONFIG
from pkg.utils.path_manager.PathManager import PathManager
from datetime import datetime
from pkg.utils.Singleton import Singleton


class AIHandler(LoggingHandler, metaclass=Singleton):
    instrumentation_key = None
    telemetry_channel = None

    def __init__(self):
        """
            Create instances of Application Insights handler based on config supplied.

            config:
                instrumentation_key

            Returns:
                object: Application handler

        """
        self.instrumentation_key = CONFIG.logging.instrumentation_key
        self.telemetry_channel = channel.TelemetryChannel()
        self.run_id = PathManager.get_working_directory() + "_" + str(datetime.timestamp(datetime.now())).split('.')[0]
        super().__init__(self.instrumentation_key, telemetry_channel=self.telemetry_channel)
        # Report exceptions To Application Insights
        enable_exceptions(self.instrumentation_key, telemetry_channel=self.telemetry_channel)

    def emit(self, record):
        """
            Emit a record.

            If a formatter is specified, it is used to format the record. If exception information is present, an Exception
            telemetry object is sent instead of a Trace telemetry object.

            Args:
                record (:class:`logging.LogRecord`). the record to format and send.
        """
        # selecting the module name if the logger is outside the function
        if record.funcName == "<module>":
            record.funcName = record.module

        # the set of properties that will ride with the record
        properties = {
            'process': record.processName,
            'module': record.module,
            'fileName': record.filename,
            'lineNumber': record.lineno,
            'level': record.levelname,
            'run_id': self.run_id,
            'functionName': record.funcName
        }

        if "exec_time" in record.__dict__.keys():
            properties["exec_time"] = record.exec_time
        if "function_name" in record.__dict__.keys():
            properties["functionName"] =  record.function_name
        if "file_name" in record.__dict__.keys():
            properties["fileName"] =  record.file_name
        if "module_name" in record.__dict__.keys():
            properties["module"] = record.module_name

        # if we have exec_info, we will use it as an exception
        if record.exc_info:
            self.client.track_exception(*record.exc_info, properties=properties)
            return

        # if we don't simply format the message and send the trace
        formatted_message = self.format(record)
        self.client.track_trace(formatted_message, properties=properties, severity=record.levelname)
