import logging
from config import CONFIG
from pkg.utils.Singleton import Singleton
from pkg.utils.path_manager.PathManager import PathManager as PH


class FileHandler(logging.FileHandler, metaclass=Singleton, ):
    logName = None

    def __init__(self):
        """
            Create instance of File Handler based on config supplied.

            config:
                filename

            Returns:
                object: File handler object

        """
        self.logName = PH.get_full_path_from_root(CONFIG.logging.filename)
        super().__init__(self.logName)
