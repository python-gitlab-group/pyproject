import logging
import importlib
from logging import StreamHandler
# TODO - Constant should be caps.
from config import CONFIG
from pkg.logging.TqdmToLogger import TqdmToLogger
from pkg.utils.Singleton import Singleton


class LogManager(metaclass=Singleton):
    handlers = None

    @classmethod
    def get_logger(cls, name):
        """
        Creates Logger instance.

        Args:
            name(str): Name of the Logger object (usually file name).

        Returns:
            logging.logger: Logger instance.
        """
        logging.basicConfig(format=CONFIG.logging.format,
                            level=CONFIG.logging.level,
                            handlers=cls.get_handlers())
        logger = logging.getLogger(name)
        logger.debug("Logger created - {}".format(name))
        return logger

    @classmethod
    def get_tqdm_logger(cls, name):
        """
        Creates Tqdm Logger instance.
        Usage:
            tqdm_out = LM.get_tqdm_logger(__name__)
            for x in tqdm(range(100), file=tqdm_out):
                time.sleep(0.2)
        Args:
            name (str): Name of the Logger object (usually file name).

        Returns:
            logging.logger: Logger instance.
        """
        return TqdmToLogger(cls.get_logger(__name__))

    @classmethod
    def get_handlers(cls):
        """
        Create instances of handlers based on config supplied. StreamHandler is always present.
        Returns:
            list: List of handler instances
        """
        if cls.handlers is not None:
            # No need to create instances every time if we already did it before.
            return cls.handlers

        # Create handler instances. Intentionally not caching any errors here.
        # Force keep StreamHandler.
        handler_instances = [StreamHandler()]
        handler_strs = CONFIG.logging.handlers.split(",")

        handlers = importlib.import_module("pkg.logging.handlers")
        for handler_str in handler_strs:
            _class = getattr(handlers, handler_str.strip())
            handler_instance = _class()
            handler_instances.append(handler_instance)

        cls.handlers = handler_instances
        return cls.handlers
