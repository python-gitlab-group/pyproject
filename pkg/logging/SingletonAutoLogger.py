from pkg.utils.Singleton import Singleton
from pkg.logging.AutoLogger import AutoLogger


class SingletonAutoLogger(Singleton, AutoLogger):
    """
    Class that inherits both Singleton and AutoLogger.
    To use, just mark this class as metaclass of the target class.
    """
    pass
