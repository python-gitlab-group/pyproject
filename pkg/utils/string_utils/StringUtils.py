class StringUtils:
    """
        This class will help in managing string values.
    """
    @staticmethod
    def right(s: str, amount: int) -> str:
        return s[-amount:]

    @staticmethod
    def left(s: str, amount: int):
        return s[:amount]

    @staticmethod
    def mid(s: str, offset, amount: int):
        return s[offset:offset + amount]
