"""
Contains the functionality to aid in finding the path of modules, files, or directories.
"""
from pkg.utils.string_utils.StringUtils import StringUtils
import os
from typing import List
import shutil
import rootpath
import pathlib
from pathlib import Path


class PathManager:
    """
        This class will help in finding the path of module, file or directory.
    """

    @staticmethod
    def create_directory_if_not_exist(relative_path: str) -> str:
        """
            Create directories if not present. This supports recursive directory creation.
            Args:
                relative_path (str): Relative path to root of the project.
            Returns:
                str: Absolute path of the directory.
        """
        # Remove last slash as it is redundant.
        if relative_path[-1] == "/":
            relative_path = relative_path[:-1]
        if not os.path.exists(PathManager.get_full_path_name(relative_path)):
            # Relative path and recursive request.
            if (relative_path[0] != "/") and ("/" in relative_path):
                paths = relative_path.split("/")
                path = "/".join(paths[:-1])
                if not os.path.exists(PathManager.get_full_path_name(path)):
                    PathManager.create_directory_if_not_exist(path)
            os.mkdir(PathManager.get_full_path_name(relative_path))
        return PathManager.get_full_path_name(relative_path)

    @staticmethod
    def delete_directory_if_exists(relative_path: str) -> bool:
        """
            Delete directories if present.
            Args:
                relative_path (str): Relative path to root of the project.
            Returns:
                bool: True if the file is Deleted/Doesn't exists, else False.
        """
        if os.path.exists(PathManager.get_full_path_name(relative_path)):
            shutil.rmtree(PathManager.get_full_path_name(relative_path))
        return not os.path.isdir(relative_path)

    @staticmethod
    def delete_file_if_exists(relative_path: str) -> None:
        """
            Delete file if present.
            Args:
                relative_path (str): Relative path to root of the project.
            Returns:
                None
        """
        if os.path.exists(PathManager.get_full_path_from_root(relative_path)):
            os.remove(PathManager.get_full_path_from_root(relative_path))

    @staticmethod
    def get_output_dir_name(folder_name: str) -> str:
        """
            Returns the path of the folder where output files are stored.
            Args:
                folder_name (str): pass the output folder name.
            Returns:
                str: path where the results are getting stored.
        """
        return os.path.join(os.path.abspath(os.path.join(os.getcwd(), os.pardir)), folder_name)

    @staticmethod
    def get_all_files_in_dir(dir_path: str, ext: str = None) -> list:
        """
            Returns a list of files with specified extension in a given directory.
            If ext is None, it will return all the files in a given directory.
            Args:
                dir_path (str): Path of the specified directory.
                ext (str): Return files only with this extension.

            Returns:
                list: List of files.
        """
        files = []
        # r=root, d=directories, f = files
        for r, d, f in os.walk(dir_path):
            for file in f:
                if ext:
                    if file.upper().endswith(ext.upper()):
                        files.append(os.path.join(r, file))
                else:
                    files.append(os.path.join(r, file))
        return files

    @staticmethod
    def get_full_path_name(relative_path: str) -> str:
        """
            Get absolute path for a given relative path to the root of the project.
            Args:
                relative_path (str): Relative path to root of the project.

            Returns:
                str: Absolute path.
        """
        working_dir = os.path.abspath(os.getcwd())
        return os.path.join(working_dir, relative_path)

    @staticmethod
    def get_full_file_name(relative_path: str, filename: str) -> str:
        """
            Get absolute path for a given relative path and a filename to the root of the project.
            Args:
                relative_path (str): Relative path to root of the project.
                filename (str) : Name of the file

            Returns:
                str: Absolute path.
        """
        return os.path.join(PathManager.get_full_path_name(relative_path), filename)

    @staticmethod
    def get_working_directory():
        """
            Get the parent directory.

            Returns:
                str: Parent Folder name.
        """
        working_dir = os.path.basename(os.getcwd())
        return working_dir

    @staticmethod
    def check_file_existence(file: str) -> bool:
        """
            Checks if the file exists or not.
            Args:
                file (str): file_name
            Returns:
                bool : True if File exists else False.
        """
        return os.path.isfile(file)

    @staticmethod
    def is_empty_file(file: str) -> bool:
        """
            Checks if the File is Empty or Not.
            Args:
                file (str): file name with full path.
            Returns:
                bool : True if File is empty else False.
        """
        return os.stat(file).st_size == 0

    @staticmethod
    def get_root_path(file: str) -> str:
        """
            Gets the root path of the working directory.
            Args:
                file (str): filename.

            Returns:
                str: Root path of the folder.
        """
        return rootpath.detect(file)

    @staticmethod
    def get_root_dir_name(file: str) -> str:
        """
            Gets the root folder name.
        Args:
            file: filename.

        Returns:
            str: Root folder name.
        """
        return pathlib.PurePath(rootpath.detect(file)).name

    @staticmethod
    def get_folders_having_filename(dir_path: str, file_name: str) -> [str]:
        """
            Returns a list of sub folders within dir_path that contains the file_name.
            Args:
                dir_path (str): Path of the specified directory.
                file_name (str): Filename to be searched.

            Returns:
                list: List of folders.
        """
        folders = []
        directories = (entry for entry in os.scandir(dir_path) if entry.is_dir())
        for directory in directories:
            if len(list(entry for entry in os.scandir(directory.path) if entry.name == file_name)):
                folders.append(directory.name)

        return folders

    @staticmethod
    def list_files_within_tree_with_prefix(start_path: str, prefix: str) -> List[str]:
        """
            Returns a list of paths containing files with prefix on the beginning of file name.
            Args:
                start_path (str): Path of the root directory.
                prefix (str): Prefix of the files to be found.

            Returns:
                list: List of paths having the file prefix.
        """
        files_path = []
        for root, dirs, files in os.walk(start_path):
            for file in files:
                if StringUtils().left(file.upper(), len(prefix)) == prefix.upper():
                    files_path.append(f"{root}/{file}")
        return files_path

    @staticmethod
    def get_subfolders_having_filename(dir_path: str, file_name: str) -> [[str]]:
        """
            Returns a list of a set of folders that contains the file_name.
            Args:
                dir_path (str): Path of the specified directory.
                file_name (str): Filename to be searched

            Returns:
                list: List of a list of sub folders.
        """
        folders: list = []
        base_path_set: tuple = Path(dir_path).parts
        # r=root, d=directories, f = files
        for r, d, f in os.walk(dir_path):
            for file in f:
                if file == file_name:
                    r_parts: tuple = Path(r).parts
                    folders.append([part for part in r_parts if part not in base_path_set])
        return folders

    @staticmethod
    def get_full_path_from_root(relative_path: str) -> str:
        """
            Gets the actual path  of the given relative path.
            Args:
                relative_path (str): relative path from the main directory.
            Returns:
                str: actual path of the folder.
        """
        return os.path.join(PathManager.get_root_path(__file__), relative_path)
