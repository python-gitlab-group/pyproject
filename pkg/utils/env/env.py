from pkg.logging.LogManager import LogManager as LM
import os


class EnvUtils:

    def __init__(self):
        self.logger = LM.get_logger(__name__)

    @staticmethod
    def get_string(env_var: str, default_value: str) -> str:
        value = os.getenv(env_var)

        if value == "" and len(default_value) > 0:
            value = default_value

        return value

    @staticmethod
    def get_int(env_var: str, default_value: int) -> int:
        value_str = os.getenv(env_var)
        if value_str != "":
            return int(value_str)

        return default_value

    def check_required(self, env_vars: list):
        for v in env_vars:
            if os.getenv(v) == "":
                self.logger.error(f"Environment variable {v} is required")
            self.logger.info(f"Environment variable {v} is ok.")
