from pkg.utils.env.env import EnvUtils
import pytest
import os


def test_get_string():
    os.environ['VAR_TEST'] = 'custom_value'

    assert 'custom_value' == EnvUtils.get_string('VAR_TEST', "default")

    os.environ['VAR_TEST'] = ''
    assert 'default' == EnvUtils.get_string('VAR_TEST', "default")


def test_get_int():
    os.environ['VAR_TEST'] = '10'

    assert 10 == EnvUtils.get_int('VAR_TEST', 20)

    os.environ['VAR_TEST'] = ''
    assert 20 == EnvUtils.get_int('VAR_TEST', 20)
