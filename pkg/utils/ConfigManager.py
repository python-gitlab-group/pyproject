"""
ConfigManager Module PyProject.

Classes:
    ConfigManager(metaclass=Singleton):
        Main class of ConfigManager module in PyProject.
"""
import json
import os
from typing import Optional

from python_json_config import ConfigBuilder
from pkg.utils.Singleton import Singleton
from pkg.utils.path_manager.PathManager import PathManager

builder = ConfigBuilder()
builder.set_field_access_required()


class ConfigManager(metaclass=Singleton):
    """
    Main class of ConfigManager util.
    """
    WORKING_DIR = PathManager.get_root_path(__file__)
    config = None
    test_config = None

    def __init__(self):
        """
            Method to instantiate Config Manager to Support Service specific and Default configs.

            Usage:
                1. when the class is called from controller to add service configs

                    from utils.ConfigManager import ConfigManager
                    ConfigManager().append_service_config("service_name")

                2. Once Controller has added the service config then from anywhere in the project:

                    from config import CONFIG
                    CONFIG.property -> default configs
                    CONFIG.property.key -> default nested configs
                    CONFIG.service_name.property -> service based configs
                    CONFIG.service_name.property.key -> service based nested configs
        """
        self.__create_default_config()
        # TODO: Currently this is duplicated as Application Insights needs to updated before the serivce config.
        # TODO: Improve this to update logging configs after service configs are added to default.
        self.append_env_config()

    def append_service_config(self, service: str, locale: Optional[str] = None) -> None:
        """
            Method to add service level configs.
        Args:
            service (str) : Service name.
            locale (Optional[str]) : Service locale.
                                     If specified, load the geographic-specific config for the given service.
        Returns:
            None
        """
        config_filename = service.lower()

        if locale:
            config_filename += f"_{locale.lower()}"

        config_file = PathManager.get_full_file_name(self.WORKING_DIR, f"configs/services/{config_filename}.json")

        if PathManager.check_file_existence(config_file):
            self.__replace_configs(config_file)
        else:
            raise FileNotFoundError(f"configs/services/{config_filename}.json file doesn't exist")

    def __create_default_config(self) -> None:
        """
            Method to create a config object with default configs.
        Returns:
            None
        """
        self.config = builder.parse_config(PathManager.get_full_file_name(self.WORKING_DIR, "configs/Default.json"))

    def append_test_config(self) -> None:
        """
            Method to append test config.

        Returns:
            None

        """
        # TODO 29812: Delete this method while moving Tell UTs to Tell directory.
        config_file = PathManager.get_full_file_name(self.WORKING_DIR, "configs/Unittest.json")
        self.__replace_configs(config_file)

    def append_service_test_config(self, service: str) -> None:
        """
        Method to append test config present inside service directory.

        Args:
            service (str): Argument mentioning Service Name.
                     When a valid Service Name is provided, "configs/Unittest.json" under the Service directory is read.

        Returns:
            None

        """
        config_file = PathManager.get_full_file_name(self.WORKING_DIR, f"{service.lower()}/configs/Unittest.json")

        if PathManager.check_file_existence(config_file):
            self.___update_configs(config_file)
        else:
            raise FileNotFoundError("{}/configs/Unittest.json file doesn't exist".format(service))

    def upsert_config(self, new_config: dict):
        """
            Method to add runtime configs.
        Args:
            new_config (dict) : Config to add.
        Returns:
            ConfigManager : The ConfigManager for chaining of calls.
        """
        if type(new_config) is not dict:
            raise ValueError(
                f"Expected dict value for new_config but got {type(new_config)}")

        self.__upsert_config(new_config=new_config)
        return self

    def replace_config_from_file(self, config_file_path: str):
        """
        Method to update and replace in the main config from a json file. If a key's value in the main
        config is a nested dict, that whole value is replaced by the incoming value (whether a dict or
        a value of some other type). This allows nested groups of configs to be replaced easily.
        Args:
            config_file_path (str) : Path to config file source.
        Returns:
            None
        """

        if PathManager.check_file_existence(config_file_path):
            self.__replace_config_from_file(config_file_path=config_file_path)
        else:
            raise FileNotFoundError(f"{config_file_path} file doesn't exist")

    def __replace_config_from_file(self, config_file_path: str) -> None:
        """
        Update/replace operation on the config from a given config file path.
        If a key matches between the main config and the incoming config, and the value is a nested dict,
        the entire nested dict in the main config is replaced with the incoming dict. If the keys match
        and the incoming value is not a dict, the value is simply updated. Any new keys from the incoming
        dict are automatically appended.
        Args:
            config_file_path(str) : Path of the config file to be read.
        Returns:
            None
        """
        new_config = ConfigManager.read_config_from_file(config_file_path=config_file_path)
        self.__replace_config(new_config=new_config)

    def __replace_config(self, new_config: dict) -> None:
        """
        Update/replace operation on the config from an incoming config dict.
        If a key matches between the main config and the incoming config, and the value is a nested dict,
        the entire nested dict in the main config is replaced with the incoming dict. If the keys match
        and the incoming value is not a dict, the value is simply updated. Any new keys from the incoming
        dict are automatically appended.
        Args:
            new_config(dict) : New config dict to be used as a source for the update/replace operation.
        Returns:
            None
        """
        existing_keys = self.config.to_dict().keys()
        for key, value in new_config.items():
            if type(value) is dict and (key in existing_keys):
                for k, v in value.items():
                    self.config.get(key).update(k, v)
            else:
                self.config.update(key, value, True)

    def __upsert_config_from_file(self, config_file_path: str) -> None:
        """
        Upsert operation on the config from a given config file path.
        This method accomplishes a straight upsert from the incoming config dict to the main config. Any matching keys
        trigger an update regardless of value type, and any new keys are appended to the main config.
        Args:
            config_file_path(str) : Path of the config file to be read.
        Returns:
            None
        """
        new_config = ConfigManager.read_config_from_file(config_file_path=config_file_path)
        self.__upsert_config(new_config=new_config)

    def __upsert_config(self, new_config: dict) -> None:
        """
        Upsert operation on the config from a given config dict.
        This method accomplishes a straight upsert from the incoming config dict to the main config. Any matching keys
        trigger an update regardless of value type, and any new keys are appended to the main config.
        Args:
            new_config(dict) : New config dict to be used as a source for the upsert operation.
        Returns:
            None
        """
        for key, value in self.__get_recursive_key_paths_values(d=new_config):
            self.config.update(key, value, True)

    def append_env_config(self) -> None:
        """
            Update config with env variables. The env variables should be set using the dot operator.
            For example: config.logging.level
            Note: Most OS allows only strings to be set as env variables. Be aware of that. If this is a
            concern we can do some crazy type manipulation here. But let's avoid it until it is absolutely
            necessary.
            Note: This method is automatically called for you via controller. You shouldn't need to call this
            method manually ever.
        Returns:
            None
        """
        for key in os.environ:
            # TODO: this needs to be fixed for windows. works fine on linux. as updating os.environ will not update
            # TODO: the system environment in windows
            if key.startswith('config.'):
                self.config.add(key.replace('config.', '').lower(), os.environ[key])

    def __get_recursive_key_paths_values(self, d: dict, path_prefix: str = '') -> tuple:
        """
            Method to get dotted path string of all keys and corresponding values in a nested dictionary.
        Args:
            d (dict): Input dictionary for which the key paths and values need to be returned.
            path_prefix (str): Optional string containing parent key name with dot.
        Returns:
            tuple: Tuple containing dotted path string of key and its value.
        """
        for key, value in d.items():
            if type(value) is dict:
                yield from self.__get_recursive_key_paths_values(value, path_prefix + key + '.')
            else:
                yield path_prefix + key, value

    def __read_configs(self, config_file) -> dict:
        """
        Method to read the config from config file.
        Args:
            config_file (str) : Path of the config file to be read.
        Returns:
            dict: Json content of the file.
        """
        with open(config_file) as f:
            json_values = json.load(f)

        return json_values

    def __replace_configs(self, config_file: str) -> None:
        """
        If the main config and the config file read have the same key, its corresponding value as a whole is replaced
        with that of main config.
        Args:
            config_file(str) : Path of the config file to be read.
        Returns:
            None
        """
        json_values = self.__read_configs(config_file)

        existing_keys = self.config.to_dict().keys()
        for key, value in json_values.items():
            if type(value) is dict and (key in existing_keys):
                for k, v in value.items():
                    self.config.get(key).update(k, v)
            else:
                self.config.update(key, value)

    def upsert_config_from_file(self, config_file_path: str):
        """
        Method to update and append (upsert) to the main config from a json file.
        When individual keys are matched between the main config and the new file, the value is updated.
        New key/value pairs are appended to the main config.
        Args:
            config_file_path (str) : Path to config file source.
        Returns:
            None
        """

        if PathManager.check_file_existence(config_file_path):
            self.__upsert_config_from_file(config_file_path=config_file_path)
        else:
            raise FileNotFoundError(f"{config_file_path} file doesn't exist")

    @staticmethod
    def read_config_from_file(config_file_path: str) -> dict:
        """
        Static method to read a config from config file into a dict.
        Args:
            config_file_path(str) : Path of the config file to be read.
        Returns:
            dict: Json content of the file.
        """
        with open(config_file_path) as f:
            json_values = json.load(f)

        return json_values

    def ___update_configs(self, config_file) -> None:
        """
        If the main config and the config file read have the same keys, only common values are replaced and
        new values are appended to the main config.
        Args:
            config_file(str) : Path of the config file to be read.
        Returns:
            None
        """
        json_values = self.__read_configs(config_file)

        for key, value in self.__get_recursive_key_paths_values(json_values):
            self.config.update(key, value, True)
