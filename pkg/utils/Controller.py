import argparse

from Service import Service as BaseService
from pkg.utils.Singleton import Singleton
from Process import Process as BaseProcess
from pkg.logging import LogManager as LM
from pkg.utils.ConfigManager import ConfigManager


class Controller(metaclass=Singleton):
    # parser and logger objects
    parser = None
    logger = None

    def __init__(self):
        """
        Initializes parser and logger objects. Sets the structure of arguments passed from command line.
        """
        self.parser = argparse.ArgumentParser()
        self.__set_arg_structure()

        self.logger = LM.get_logger(__name__)

    def perform_validation(self, service: str, process_list: list, locale: str = None) -> bool:
        """
        This function validates the service or process by checking if it is present in the list of valid services or processes.
        Args:
            service: Name of service
            process_list: Name of process
            locale: (str) optional locale
        Returns:
             bool: True if valid, False if invalid
        """
        # Validate service
        valid_service_list = BaseService.get_all_services()

        if service.lower() in valid_service_list:

            # Validate process
            valid_process_list = BaseService.get_all_processes(service)

            for process in process_list:
                if process.lower() not in valid_process_list:
                    self.logger.error(f"Invalid process name {process}")
                    return False

            # Validate locale
            valid_locale_list = BaseService.get_all_locales(service)

            if not locale and valid_locale_list:
                self.logger.error(f"Locale not provided for service {service}, "
                                  f"please specify one of {valid_locale_list}")
                return False

            if locale and locale.lower() not in valid_locale_list:
                self.logger.error(f"Invalid locale ({locale.lower()}) provided for service {service}. "
                                  f"Available locales are {valid_locale_list}")
                return False

            return True

        else:
            self.logger.error(f"Invalid service name {service}")
            return False

    def get_args(self) -> argparse.Namespace:
        """
        Parses arguments
        Returns:
            parser.parse_args(): returns this method that helps in parsing an argument.
        """
        return self.parser.parse_args()

    def __set_arg_structure(self) -> argparse.ArgumentParser:
        """
        The service and process elements are added as arguments to parser
        Returns:
            utils.Controller: parser object with arguments added
        """
        process_group = BaseService.get_all_service_groups()
        services = BaseService.get_all_services()
        locale_group = BaseService.get_all_locale_groups()

        self.parser.add_argument('--service', required=True, type=str, help='Please specify the service to run. '
                                                                            + str(services))
        self.parser.add_argument('--processes', required=True, type=str, help='Please specify the process names. '
                                                                              + str(process_group))
        self.parser.add_argument('--locale', required=False, type=str, help='Please specify the locale. '
                                                                            + str(locale_group))
        return self.parser

    def execute_processes(self) -> None:
        """
        Executes all the processes passed for service by calling their respective
        classes and its execute_process() methods
        Returns:
            None
        """
        service = self.get_args().service
        processes = self.get_args().processes.split(',')
        locale = self.get_args().locale

        if self.perform_validation(service, processes, locale=locale):
            ConfigManager().append_service_config(service, locale=locale)
            ConfigManager().append_service_dm_config(service)
            ConfigManager().append_env_config()

            for process in processes:
                process_class = BaseProcess().get_process_instance(service, process)
                process_class.execute_process()
        else:
            raise Exception("Invalid values passed to controller.")

    def get_parser(self) -> argparse.ArgumentParser:
        """
            method to get the parser object
        Returns:
            parser(argparse.ArgumentParser): parser object created in this class
        """
        return self.parser
