from pyspark.sql import SparkSession
from pyspark.sql.dataframe import DataFrame
from pkg.logging.LogManager import LogManager as LM


class FileSparkManager:
    """
    Utility class for manage files with PySpark.
    """

    def __init__(self) -> None:
        self.logger = LM.get_logger(__name__)
        self.spark_session = SparkSession.builder.getOrCreate()

    def open_csv_file(self, path: str, delimiter: str, encoding: str = "ISO-8859-1", header: bool = False) -> DataFrame:
        """
        Method that open csv file and return it as a Spark dataframe.
        Args:
            path (str): File path.
            delimiter (str): File delimiter
            encoding (str): File encoding.
            header (bool): When first row of the file is the header.
        Returns:
            DataFrame: Returns PySpark DataFrame on fetching the data else raises Exception.
        """
        try:
            df = self.spark_session.read.option("header", header).option("delimiter", delimiter).option("encoding",
                                                                                                        encoding).csv(
                path)
        except Exception as err:
            self.logger.error(f"could not open {path} due to {err}")
            raise err

        return df
