from pkg.utils.string_utils.StringUtils import StringUtils
from pkg.utils.path_manager.PathManager import PathManager
from pkg.logging.LogManager import LogManager as LM
from shutil import move
import zipfile
import requests


class FileManager:
    """
        This class will help in managing files.
    """
    def __init__(self):
        self.logger = LM.get_logger(__name__)
        self.su = StringUtils()

    def get_file_from_url(self, url: str, save_path: str, chunk_size: int = 128):
        """
            Download file from a given URL.
            Args:
                url (str): URL to request the file to the server.
                save_path (str): Relative path to the save the file. I.e. './file/sample.zip'.
                chunk_size (int): Chunk size to avoid reading the content at once into memory.
        """
        self.logger.info(f"Getting file from {url}")
        r = requests.get(url, stream=True)
        with open(save_path, 'wb') as fd:
            for chunk in r.iter_content(chunk_size=chunk_size):
                fd.write(chunk)

    def unpack_files(self, path: str, unpack_to: str) -> None:
        """
            Unpack zip files to a given folder.
            Args:
                path (str): Relative path to the zip files location.
                unpack_to (str): Relative path to the zip files location.
        """
        self.logger.info(f"Unpacking files fom {path}")
        files = PathManager.get_all_files_in_dir(path, 'zip')

        path = self.handle_path_slash(path, False)
        PathManager.create_directory_if_not_exist(f"{path}{unpack_to}")
        for file in files:
            if zipfile.is_zipfile(file):
                zip_reference = zipfile.ZipFile(file)
                zip_reference.extractall(f"{path}{unpack_to}")

    @staticmethod
    def separate_required_files(path: str, prefix: str, destiny: str):
        """
            Copy required data to a separate path.

            Args:
                path (str): Relative path to the root directory of data files.
                prefix (str): Prefix of files to be found.
                destiny (str): Destiny directory for land the found files.
        """
        PathManager.create_directory_if_not_exist(f"{path}/{destiny}")
        found_paths = PathManager.list_files_within_tree_with_prefix(path, prefix)
        for file in found_paths:
            file_name = file.split("/", file.count("/"))[-1]
            to_path = f"{path}/{destiny}/{file_name}"
            move(file, to_path)

    def handle_path_slash(self, path: str, remove_slash: bool) -> str:
        """
            Add or remove slash from the end of a given string path.
            Args:
                path (str): Path to be transformed.
                remove_slash (bool): Indicates the action i.e. True == Remove Slash.
            Returns:
                str: Path with or without slash if remove_slash true, otherwise path with slash.
        """
        if remove_slash and self.su.right(path, 1) == '/':
            return path[0:len(path)-1]

        if not remove_slash and self.su.right(path, 1) != '/':
            return f"{path}/"

        return path
