"""
Access data from Postgresql with Spark.
"""
from pyspark.sql.dataframe import DataFrame
from pyspark.sql import SparkSession
from pkg.db.Base import Base


class Postgresql(Base):
    """
    Utility class for accessing Postgresql using PySpark.
    """

    def __init__(self) -> None:
        """
        Method to instantiate Postgresql class.
        """
        super().__init__()

        self.spark_session = SparkSession.builder.config('spark.jars', './configs/postgresql-42.2.20.jar').config(
            "spark.driver.memory", "10g").getOrCreate()
        self.url = f"jdbc:postgresql://{self.server}:{self.port}/{self.database}"

        # DB Properties
        self.db_properties = {'user': self.username, 'password': self.password, 'driver': self.driver}

    def select(self, query: str, retry: bool = True) -> DataFrame:
        """
        Method that supports executing select queries in Postgresql and returns a spark DataFrame.
        Args:
            query (str): SQL query to be passed into read method.
            retry (bool): Whether to retry in case of failure or not.
        Returns:
            DataFrame: Returns PySpark DataFrame on fetching the data else raises Exception.
        """
        try:
            self.logger.debug(f"<<<< Executing Query -- {query}")
            df = self.spark_session.read.format("jdbc") \
                .option("url", self.url) \
                .option("query", query) \
                .option("user", self.username) \
                .option("password", self.password) \
                .load()
            self.logger.debug(">>>> Executed Query ")
        except Exception as err:
            if retry:
                self.logger.warning(f" Could not fetch the data due to {err}, retrying {retry}")
                self.select(query, retry=False)
            self.logger.error(f" Could not fetch the data due to {err}")
            raise err
        return df

    def insert_spark_df_to_table(self, sdf: DataFrame, qualified_table: str, show_counts: bool,
                                 write_mode: str = 'overwrite') -> None:
        """Save a Spark Dataframe as to a Postgresql table.

            Args:
              sdf (DataFrame): Spark Dataframe to save to Postgresql.
              qualified_table (str): Qualified table name. E.g: "ingestion.census".
              show_counts (bool): Verbose mode to show some statistics.
              write_mode (str): Mode operation to save e.g: 'append', 'overwrite'.

            Returns:
              None

            Attention:
              The default write_mode default is 'overwrite'. Make sure you want it.

        """
        self.logger.info(f"Saving into {qualified_table}.")
        try:
            sdf.write.jdbc(url=self.url, table=qualified_table, mode=write_mode, properties=self.db_properties)
        except Exception as err:
            self.logger.error(f"Could not insert to {qualified_table} due to {err}")
            raise err

        if show_counts:
            self.logger.info(f"Number of Rows: {sdf.count()}")
            self.logger.info(f"Number of Columns: {len(sdf.columns)}")

    def read_from_db(self, qualified_table: str) -> DataFrame:
        """
        Method that read a given qualified table from Postgresql and returns a spark DataFrame.
        Args:
            qualified_table (str): Qualified table name. E.g: "ingestion.census".
        Returns:
            DataFrame: Returns PySpark DataFrame on fetching the data else raises Exception.
        """
        try:
            sdf = self.spark_session.read.jdbc(url=self.url, table=qualified_table, properties=self.db_properties)
        except Exception as err:
            self.logger.error(f" Could not fetch the data due to {err}")
            raise err

        return sdf
