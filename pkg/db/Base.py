from pkg.logging.LogManager import LogManager as LM
from config import CONFIG


class Base:

    def __init__(self):
        self.logger = LM.get_logger(__name__)
        self.dp_type = CONFIG.db.type

        # the following variables must be encrypted env vars on production
        self.server = CONFIG.db.server
        self.port = CONFIG.db.port
        self.database = CONFIG.db.database
        self.username = CONFIG.db.username
        self.password = CONFIG.db.password
        self.driver = CONFIG.db.driver
