import importlib
from pkg.logging.LogManager import LogManager as LM


class Process:
    """
    Base process class.
    """
    logger = None

    def __init__(self):
        """
        Initialisation method of Process class.
        """
        self.logger = LM.get_logger(__name__)

    @staticmethod
    def get_process_instance(service, process):
        """
        Creates instance of particular process.
        Args:
            service(str): Name string of the service.
            process(str): Name string of the process.
        Returns:
            process_instance(<service>.<process>): process_instance of the input process from the input service.
        """
        process = importlib.import_module(f"{service}.{process}")
        process_class = getattr(process, "Process")
        return process_class()
